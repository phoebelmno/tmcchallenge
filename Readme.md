Welcome to the Mindful Chef Technical Exercise. In this exercise we are creating the sign up flow to add recipes for checkout. This starter kit is based off `create-react-app` using TypeScript. Please use the tools and technologies you feel most familiar with. For example, if you're not feeling confortable with TypeScript, feel free to use JavaScript.

Please don't spend more than 2.5 hours on this exercise. When finishing up the exercise, please make sure to have some documentation on how you approached the different tasks and why specific technical decisions have been made for example use technology X or technology Y. Make sure to use Git and please use meaningful commits.

## Task 1

Build a recipe selection form by rendering a list of recipes with their specific information such as title, description and image. Feel free to use the data in `data/recipes.json`.
Use any styling you would like. It is important that there should three recipes next to each other in a desktop resolution (`desktop.png`) and one full width on a mobile device (`mobile.png`).

## Task 2

The select box should filter the recipes by its protein and only display the selected ones.

## Task 3

Feel free to add anything that you think would improve the user experience.

# Notes having completed test

I've began by building the repo and environment using npx create-react-app. I began with the UI - creating a recipe card with static text, then building out more and making sure they were responsive: 1 card for mobile, 2 for tablet and 3 for mobile.

I then took out the static text and had load the recipe titles, description and images from the json object.

I added a little more styling - and if I had more time I would have liked to add more - particularly to the select dropdown.

Conscious of time, I then wanted to make sure the right recipes would display based on the selected filter - so I used an onChange event on the select component to set the protein state, which would then update the displayed recipes.

I enjoyed working on this challenge and would have liked to spend longer but was mindful to keep it to 2.5 hours (I think I spent closer to 3.5 all in). If I could spend longer I would have liked to add a header, more styling, and also a visual to make it clear which recipes were being displayed.