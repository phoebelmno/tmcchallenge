import React, {useState, useEffect} from 'react';
import RecipeCard from './components/RecipeCard/RecipeCard';
import FilterSelector from './components/FilterSelector/FilterSelector';
import { createTheme, ThemeProvider } from "@material-ui/core/styles"
import Grid from '@material-ui/core/Grid'
import "./styles.css";
import recipes from "./data/recipes.json";

// Customizable breakpoints for responsive layout
const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 768,
      lg: 1200,
      xl: 1536,
    },
  },
});


function App() {

  const [protein, setProtein] = useState('all');
  const [displayedRecipes, setDisplayedRecipes] = useState(recipes);

  useEffect(() => {
    if(protein === 'all') {
      setDisplayedRecipes(recipes);
    } else if (protein === 'meatAndFish') {
      setDisplayedRecipes(recipes.filter((recipe) => recipe.protein.includes('meat') || recipe.protein.includes('fish')));
    } else if (protein === 'fish') {
      setDisplayedRecipes(recipes.filter((recipe) => recipe.protein.includes('fish')));
    } else if (protein === 'vegan') {
      setDisplayedRecipes(recipes.filter((recipe) => recipe.protein.includes('vegan')));
    }
    
  }, [protein]) 

  return (
    // console.log(recipes),
    <div className="App">
      <ThemeProvider theme={theme}>
        <Grid container spacing={3} className="recipeDisplay">
          <Grid item xs={12}>
            <FilterSelector onChange={e => {setProtein(e.target.value)}}/>
          </Grid>
          {displayedRecipes.map((recipe) => (
            <Grid item xs={12} sm={6} md={4}>
              <RecipeCard image={recipe.image} title={recipe.title} description={recipe.description}/>
            </Grid>
          ))}
        </Grid>
      </ThemeProvider>

    </div>
  );
}

export default App;
