export default {
  recipeCard: {
    border: '2px solid #e6e6e6',
    background: 'white',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    color: 'rgb(87, 79, 119)',
    height: '100%',
  },
  
  image: {
    width: '100%'
  }
}