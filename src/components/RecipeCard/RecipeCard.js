import React from 'react';
import RecipeCardContent from '../RecipeCardContent/RecipeCardContent';
import styles from './styles'

const RecipeCard = props => {
  return (
    <>
      <div className="recipeCard" style={styles.recipeCard}>
        <img src={props.image} style={styles.image}></img>
        <RecipeCardContent title={props.title} description={props.description}
        />
      </div>
    </>
  )
}

export default RecipeCard;