import React from 'react';

const FilterSelector = ({onChange}) => {

  return (
    <>
      <select onChange={onChange}>
        <option value="all">All healthy recipes</option>
        <option value="meatAndFish">Meat and fish only</option>
        <option value="fish">Pescatarian only</option>
        <option value="vegan">Vegan only</option>
      </select>
    </>
  )
}

export default FilterSelector;