export default {
  recipeCardContent: {
    padding: '20px',
    position: 'relative',
  },
  moreInfo: {
    position: 'absolute',
    top: '-3px',
    right: '-3px',
    zIndex: 1,
    display: 'flex',
    alignItems: 'center',
    fontSize: '14px',
    fontStyle: 'italic',
    fontWeight: 'bold',
    color: 'rgb(247, 155, 46)',
  },
  moreInfoIcon: {
    fill: 'rgb(247, 155, 46)',
  },
}