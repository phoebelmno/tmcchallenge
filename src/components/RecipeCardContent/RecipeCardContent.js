import React from 'react';
import styles from './styles'

const RecipeCardContent = (props) => {
  return (
    <>
      <div style={styles.recipeCardContent}>
        <p style={styles.moreInfo}>
          more info
          <svg height="25" viewBox="0 0 48 48" width="48" xmlns="http://www.w3.org/2000/svg" style={styles.moreInfoIcon}><path d="M0 0h48v48H0z" fill="none"></path><path d="M24 4C12.95 4 4 12.95 4 24s8.95 20 20 20 20-8.95 20-20S35.05 4 24 4zm2 30h-4V22h4v12zm0-16h-4v-4h4v4z"></path></svg></p>
        <h3>{props.title}</h3>
        <p>{props.description}</p>
      </div>
    </>
  )
}

export default RecipeCardContent;